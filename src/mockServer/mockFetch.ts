const MIN_WAIT_MS = 100;
const MAX_WAIT_MS = 2000;

const randomInterval = () =>
	MIN_WAIT_MS + Math.round(Math.random() * (MAX_WAIT_MS - MIN_WAIT_MS));

const mockFetch = <Result>(result?: Result) =>
	new Promise<Result>(resolve =>
		setTimeout(() => resolve(result), randomInterval())
	);

export default mockFetch;
