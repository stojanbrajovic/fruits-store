import mockFetch from "./mockFetch";
import FruitRow from "../types/FruitRow";
import createFruitInfos from "./createFruitInfos";

const fruits = createFruitInfos();

const MAX_SALES = 1000;
const DAY_MS = 24 * 60 * 60 * 1000;

const getSalesRow = (date: Date): FruitRow => ({
	date,
	fruitsSold: fruits.reduce(
		(res, { name }) => ({
			...res,
			[name]: Math.round(Math.random() * MAX_SALES)
		}),
		{} as Record<string, number>
	)
});

const getTomorrow = (date: Date) => new Date(date.getTime() + DAY_MS);

const getReport = (startDate: Date, endDate: Date) => {
	const result = [getSalesRow(startDate)];

	while (1) {
		const newDate = getTomorrow(result[result.length - 1].date);
		if (newDate > endDate) {
			break;
		}
		result.push(getSalesRow(newDate));
	}

	return mockFetch(result);
};

export default getReport;
