import FruitInfo from "../types/FruitColor";

const infos: FruitInfo[] = [
	{ name: "Bananas", color: "yellow" },
	{ name: "Apples", color: "red" },
	{ name: "Strawberries", color: "lightcoral" },
	{ name: "Oranges", color: "orange" }
];

const createFruitInfos = () => [...infos];

export default createFruitInfos;
