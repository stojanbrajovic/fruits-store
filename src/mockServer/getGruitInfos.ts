import createFruitInfos from "./createFruitInfos";
import mockFetch from "./mockFetch";

const getFruitInfos = () => {
	return mockFetch(createFruitInfos());
};

export default getFruitInfos;
