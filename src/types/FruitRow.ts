type FruitRow = {
	date: Date;
	fruitsSold: Record<string, number>;
};

export default FruitRow;
