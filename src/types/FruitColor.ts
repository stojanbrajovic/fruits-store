type FruitInfo = {
	name: string;
	color: string;
};

export default FruitInfo;
