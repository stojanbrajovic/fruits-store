const dateToString = (date: Date) => date.toISOString().substr(0, 10);

export default dateToString;
