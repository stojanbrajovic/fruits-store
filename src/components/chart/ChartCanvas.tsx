import * as React from "react";
import Canvas, { PaintFunction, CommonCanvasProperties } from "./Canvas";
import {
	paintGrid,
	GridColors,
	PaintGridProps
} from "./paintFunctions/paintGrid";

export type ChartCanvasGrid = { horizontal: number[]; vertical: number[] };

export interface ChartCanvasProperties extends CommonCanvasProperties {
	grid?: ChartCanvasGrid;
	paintBelowGridFunctions?: PaintFunction[];
	paintAboveGridFunctions?: PaintFunction[];
	gridColors?: string | GridColors;
}

const ChartCanvas: React.SFC<ChartCanvasProperties> = props => {
	const paintChartGrid = (ctx: CanvasRenderingContext2D) => {
		paintGrid(ctx, {
			...(props as PaintGridProps),
			horizontalLines: (props.grid || {}).horizontal,
			verticalLines: (props.grid || {}).vertical
		});
	};

	return (
		<Canvas
			paintFunctions={[
				...(props.paintBelowGridFunctions || []),
				paintChartGrid,
				...(props.paintAboveGridFunctions || [])
			]}
			{...props}
		/>
	);
};
ChartCanvas.defaultProps = {
	grid: { horizontal: [], vertical: [] },
	paintBelowGridFunctions: [],
	paintAboveGridFunctions: [],
	backgoundColor: "rgba(255,255,255,0)"
};

export default ChartCanvas;
