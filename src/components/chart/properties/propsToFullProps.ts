import { ValuePoint, ChartProperties } from "./ChartProperties";
import { ChartContainerProperties } from "../chartContainer/ChartContainer";
import { Grid, GridLine } from "../GridLine";

const getGridFromValues = <ValueType extends ValuePoint = ValuePoint>(
	realPositionValues: ValueType[],
	userValues: ValueType[]
): Grid => {
	const horizontal: GridLine[] = [];
	const vertical: GridLine[] = [];

	realPositionValues.forEach((value, index) => {
		const userValue = userValues[index];
		horizontal.push({ position: value.y, label: userValue.y + "" });
		vertical.push({ position: value.x, label: userValue.x + "" });
	});

	horizontal.sort((val1, val2) => val1.position - val2.position);
	vertical.sort((val1, val2) => val1.position - val2.position);

	return {
		horizontal: horizontal.filter(
			(val, index) =>
				index === 0 || horizontal[index - 1].position !== val.position
		),
		vertical: vertical.filter(
			(val, index) =>
				index === 0 || vertical[index - 1].position !== val.position
		)
	};
};

const getExtremesForValues = <ValueType extends ValuePoint = ValuePoint>(
	values: ValueType[]
) => {
	const max = { x: Number.MIN_VALUE, y: Number.MIN_VALUE };
	const min = { x: Number.MAX_VALUE, y: Number.MAX_VALUE };

	values.forEach(value => {
		max.y = Math.max(max.y, value.y);
		min.y = Math.min(min.y, value.y);
		max.x = Math.max(max.x, value.x);
		min.x = Math.min(min.x, value.x);
	});

	return { min, max };
};

const generateGrid = (
	minX: number,
	minY: number,
	props: ChartProperties,
	originalValues: ValuePoint[]
) => {
	const { grid } = props;
	let gridFromValues: Grid = {};
	if (!grid || !grid.horizontal || !grid.vertical) {
		gridFromValues = getGridFromValues(props.values, originalValues);
	}

	const resultGrid: Grid = { ...gridFromValues, ...grid };

	resultGrid.horizontal = (resultGrid.horizontal || []).map(line => {
		const position = line.position - (props.height ? 0 : minY);
		return {
			...line,
			position
		};
	});
	resultGrid.vertical = (resultGrid.vertical || []).map(line => {
		const position = line.position - (props.width ? 0 : minX);
		return {
			...line,
			position
		};
	});

	return resultGrid;
};

export const chartPropsToContainerProps = <ValueType = ValuePoint>(
	propsArg: ChartProperties<ValueType>,
	values: ValuePoint[]
) => {
	const props = {
		yCoordinateToString: (pos: number) => "" + Math.round(pos),
		...propsArg,
		values
	};

	const valuesExtremes = getExtremesForValues(props.values);

	const userValues = props.values.slice();
	props.values = props.values.map(value => {
		const x = value.x - (props.width ? 0 : valuesExtremes.min.x);
		const y = value.y - (props.height ? 0 : valuesExtremes.min.y);
		const label = value.label || value.x + " : " + value.y;
		return { ...(value as any), x, y, label }; // using any bacuse of https://github.com/Microsoft/TypeScript/issues/13557
	});

	const existingYCoordToString =
		props.yCoordinateToString || ((pos: number) => "" + Math.round(pos));
	props.yCoordinateToString = (pos: number) => {
		const y = pos + (props.height ? 0 : valuesExtremes.min.y);
		return existingYCoordToString(y);
	};

	props.grid = generateGrid(
		valuesExtremes.min.x,
		valuesExtremes.min.y,
		props,
		userValues
	);

	return props as ChartContainerProperties;
};
