import { Grid } from "../GridLine";
import { InfoConfig } from "./InfoConfig";
import { AdjustValuesToChartDimensionsConfig } from "../adjustValuesToChart";
import { GridColors } from "../paintFunctions/paintGrid";

export interface ValuePoint {
	x: number;
	y: number;

	label?: string;
}

export interface OptionalChartContainerProperties
	extends AdjustValuesToChartDimensionsConfig {
	height: number;
	width: number;
	grid?: Grid;

	infoConfig?: InfoConfig;

	snapDistance?: number;
	borderWidth?: number;
	yCoordinateToString?: (price: number) => string;
	isChartContainerHidden?: boolean;
}

export interface ChartProperties<ValueType = ValuePoint>
	extends OptionalChartContainerProperties {
	values: ValueType[];

	backgroundColor?: string;
	gridColors?: string | GridColors;
}

export interface ChartContainerConfig extends OptionalChartContainerProperties {
	values?: ValuePoint[];
}
