export interface SingleBarValue {
	x: number;
	high: number;
	low: number;

	open: number;
	close: number;

	label?: string;
}

export interface BarColors {
	upLineColor: string;
	downLineColor: string;
	equalLineColor: string;
}

export interface BarProperties {
	barWidth?: number;
	barColors?: BarColors;
	values: SingleBarValue[];
}

export const DEFAULT_PROPS = {
	barWidth: 5,
	barColors: {
		upLineColor: "green",
		downLineColor: "red",
		equalLineColor: "black"
	},
	values: []
};
