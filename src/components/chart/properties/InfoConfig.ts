export interface InfoConfig {
	fontSize?: number;
	textColor?: string;
	gridValuesMargin?: number;
	cursorSelectionTextColor?: string;
	cursorSelectionColor?: string;

	isCursorDisabled?: boolean;
	isRightInfoDisabled?: boolean;
	isBottomInfoDisabled?: boolean;
	snapPointRadius?: number;
}

export const defaultInfoConfig: InfoConfig = {
	gridValuesMargin: 0,
	cursorSelectionTextColor: 'white',
	cursorSelectionColor: 'darkblue'
};
