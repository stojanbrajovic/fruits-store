import { yToScreenY } from "./yToScreenY";
import { ValuePoint } from "../properties/ChartProperties";

export interface HistogramPoint extends ValuePoint {
	color?: string;
}

export interface PaintHistogramChartConfig {
	color?: string;
	barWidth?: number;
	outlineThickness?: number;
	outlineColor?: string;
}

export const DEFAULT_CONFIG = {
	color: "black",
	barWidth: 10,
	outlineThickness: 2,
	outlineColor: "darkgrey"
};

export const paintHistogram = (
	ctx: CanvasRenderingContext2D,
	values: HistogramPoint[],
	configArg: PaintHistogramChartConfig = {}
) => {
	const config = { ...DEFAULT_CONFIG, ...configArg };

	ctx.save();

	ctx.strokeStyle = config.outlineColor;
	ctx.lineWidth = config.outlineThickness;

	values.forEach(value => {
		ctx.fillStyle = value.color || config.color;
		const y = yToScreenY(ctx, value.y);
		ctx.beginPath();
		ctx.rect(
			value.x - config.barWidth / 2,
			y,
			config.barWidth,
			ctx.canvas.clientHeight + 100
		);
		ctx.fill();
		ctx.stroke();
	});

	ctx.restore();
};
