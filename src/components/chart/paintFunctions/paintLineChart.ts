import { yToScreenY } from "./yToScreenY";
import { Point } from "./Point";

export interface PaintLineChartProperties {
	lineWidth?: number;
	lineColor?: string;
}

const defaultProps = { lineColor: "black", lineWidth: 1 };

export const paintLineChart = (
	ctx: CanvasRenderingContext2D,
	points: Point[],
	propsArg: PaintLineChartProperties
) => {
	if (points.length === 0) {
		return;
	}

	const props = { ...defaultProps, ...propsArg };

	ctx.save();

	ctx.strokeStyle = props.lineColor;
	ctx.lineWidth = props.lineWidth;
	ctx.fillStyle = props.lineColor;

	ctx.beginPath();

	const firstPoint = points[0];
	const circleRadius = props.lineWidth / 2;
	ctx.arc(
		firstPoint.x,
		yToScreenY(ctx, firstPoint.y),
		circleRadius,
		0,
		2 * Math.PI
	);
	ctx.fill();

	ctx.beginPath();
	ctx.moveTo(firstPoint.x, yToScreenY(ctx, firstPoint.y));
	for (let i = 1; i < points.length; i++) {
		ctx.lineTo(points[i].x, yToScreenY(ctx, points[i].y));
		ctx.stroke();
	}

	ctx.beginPath();
	const lastPoint = points[points.length - 1];
	ctx.arc(
		lastPoint.x,
		yToScreenY(ctx, lastPoint.y),
		circleRadius,
		0,
		2 * Math.PI
	);
	ctx.fill();

	ctx.restore();
};
