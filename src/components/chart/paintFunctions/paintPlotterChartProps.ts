export interface Point {
	x: number;
	y: number;
}

export interface PaintPlotterChartProperties<PointType = Point> {
	values: PointType[];
	isHighlighted?: boolean;
	highlightColor?: string;
	highlightThickness?: number;
	color?: string;
}

export const defaultPlotterProps: PaintPlotterChartProperties = {
	values: [],
	isHighlighted: false,
	highlightColor: "aqua",
	highlightThickness: 4,
	color: "black"
};

export const yToSceenY = (ctx: CanvasRenderingContext2D, y: number) => {
	return ctx.canvas.clientHeight - y;
};
