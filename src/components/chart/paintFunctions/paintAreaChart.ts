import { yToScreenY } from "./yToScreenY";
import { Point } from "./Point";
import { paintLineChart } from "./paintLineChart";

export interface PaintAreaConfig {
	lineColor?: string;
	topColor?: string;
	bottomColor?: string;
	lineThickness?: number;
}

const createLine = (ctx: CanvasRenderingContext2D, points: Point[]) => {
	ctx.moveTo(points[0].x, yToScreenY(ctx, points[0].y));
	for (let i = 1; i < points.length; i++) {
		ctx.lineTo(points[i].x, yToScreenY(ctx, points[i].y));
	}
};

const paintArea = (
	ctx: CanvasRenderingContext2D,
	points: Point[],
	props: PaintAreaConfig
) => {
	const minY = Math.min.apply(
		null,
		points.map(point => point.y)
	);
	const gradient = ctx.createLinearGradient(
		0,
		minY,
		0,
		ctx.canvas.clientHeight
	);
	gradient.addColorStop(0, props.topColor || props.lineColor || "");
	gradient.addColorStop(1, props.bottomColor || props.lineColor || "");
	ctx.fillStyle = gradient;

	ctx.beginPath();

	createLine(ctx, points);
	ctx.lineTo(points[points.length - 1].x, ctx.canvas.clientHeight);
	ctx.lineTo(points[0].x, ctx.canvas.clientHeight);

	ctx.closePath();

	ctx.fill();
};

const DEFAULT_CONFIG: PaintAreaConfig = {
	lineColor: "red",
	topColor: "lightgrey",
	bottomColor: "lightgrey",
	lineThickness: 5
};

export const paintAreaChart = (
	ctx: CanvasRenderingContext2D,
	values: Point[],
	propsArg: PaintAreaConfig = {}
) => {
	if (!values.length) {
		return;
	}

	ctx.save();

	const props = { ...DEFAULT_CONFIG, ...propsArg };

	paintArea(ctx, values, props);

	paintLineChart(ctx, values, {
		lineColor: props.lineColor,
		lineWidth: props.lineThickness
	});

	ctx.restore();
};
