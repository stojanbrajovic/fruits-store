export const yToScreenY = (ctx: CanvasRenderingContext2D, y: number) => {
	return ctx.canvas.clientHeight - y;
};