import { yToScreenY } from "./yToScreenY";
import { BarProperties, DEFAULT_PROPS } from "../properties/BarProperties";

const paintBarsChart = (
	ctx: CanvasRenderingContext2D,
	propsArg: BarProperties
) => {
	const props: BarProperties = { ...DEFAULT_PROPS, ...propsArg };

	const {
		barWidth = DEFAULT_PROPS.barWidth,
		barColors = DEFAULT_PROPS.barColors
	} = props;
	props.values.forEach(value => {
		ctx.save();

		const lineColor =
			value.open === value.close
				? barColors.equalLineColor
				: value.close > value.open
				? barColors.upLineColor
				: barColors.downLineColor;
		ctx.strokeStyle = lineColor;
		ctx.fillStyle = lineColor;

		ctx.lineWidth = barWidth;
		ctx.beginPath();

		ctx.moveTo(value.x, yToScreenY(ctx, value.low));
		ctx.lineTo(value.x, yToScreenY(ctx, value.high));

		ctx.moveTo(value.x - barWidth * 2, yToScreenY(ctx, value.open));
		ctx.lineTo(value.x, yToScreenY(ctx, value.open));

		ctx.moveTo(value.x, yToScreenY(ctx, value.close));
		ctx.lineTo(value.x + barWidth * 2, yToScreenY(ctx, value.close));

		ctx.stroke();

		ctx.beginPath();
		[value.close, value.open].forEach(y => {
			ctx.arc(value.x, yToScreenY(ctx, y), barWidth / 2, 0, 2 * Math.PI);
			ctx.fill();
		});

		ctx.restore();
	});
};

export { paintBarsChart };
