import { yToScreenY } from "./yToScreenY";
import {
	BarProperties,
	DEFAULT_PROPS as DEFAULT_BARS_PROPS
} from "../properties/BarProperties";

export interface CandlestickProperties extends BarProperties {
	lineColor?: string;
	lineThickness?: number;
}

export const DEFAULT_PROPS = {
	...DEFAULT_BARS_PROPS,
	lineColor: "black",
	lineThickness: 1
};

const EQUAL_RECT_HEIGHT = 2;

export const paintCandlestickChart = (
	ctx: CanvasRenderingContext2D,
	propsArg: CandlestickProperties
) => {
	const props = { ...DEFAULT_PROPS, ...propsArg };

	ctx.save();

	for (let i = 0; i < props.values.length; i++) {
		ctx.save();
		const value = props.values[i];

		ctx.fillStyle =
			value.open > value.close
				? props.barColors.upLineColor
				: value.open < value.close
				? props.barColors.downLineColor
				: props.barColors.equalLineColor;

		ctx.beginPath();
		ctx.strokeStyle = props.lineColor;
		ctx.lineWidth = props.lineThickness;
		ctx.moveTo(value.x, yToScreenY(ctx, value.high));
		ctx.lineTo(value.x, yToScreenY(ctx, value.low));
		ctx.stroke();

		const rectX = value.x - props.barWidth / 2;
		const rectY = yToScreenY(ctx, value.open);
		const rectHeight = value.open - value.close || EQUAL_RECT_HEIGHT;
		ctx.fillRect(rectX, rectY, props.barWidth, rectHeight);

		ctx.strokeStyle = props.lineColor;
		ctx.strokeRect(rectX, rectY, props.barWidth, rectHeight);

		ctx.restore();
	}

	ctx.restore();
};
