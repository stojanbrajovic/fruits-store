export interface GridColors {
	vertical: string;
	horizontal: string;
}

export interface PaintGridProps {
	horizontalLines?: number[];
	verticalLines?: number[];
	lineDash?: number[];
	gridColors?: GridColors | string;
}

const defaultProps = {
	horizontalLines: [] as number[],
	verticalLines: [] as number[],
	lineDash: [1, 2],
	gridColors: "rgba(102,102,102,0.5)" as string | GridColors
};

export const paintGrid = (
	ctx: CanvasRenderingContext2D,
	propsArg: Partial<typeof defaultProps> = defaultProps
) => {
	const props = { ...defaultProps, ...propsArg };

	ctx.save();

	if (ctx.setLineDash) {
		ctx.setLineDash(props.lineDash);
	}
	ctx.beginPath();

	ctx.strokeStyle = (((props.gridColors as unknown) as GridColors).vertical ||
		props.gridColors) as string;
	props.verticalLines.forEach(line => {
		const xPos = Math.floor(line) + 0.5;
		ctx.moveTo(xPos, 0);
		ctx.lineTo(xPos, ctx.canvas.height);
	});
	ctx.stroke();

	ctx.beginPath();
	ctx.strokeStyle = (((props.gridColors as unknown) as GridColors)
		.horizontal || props.gridColors) as string;
	props.horizontalLines.forEach(line => {
		const y = ctx.canvas.height - 1 - line + 0.5;
		ctx.moveTo(1, y);
		ctx.lineTo(ctx.canvas.width - 1, y);
	});
	ctx.stroke();

	ctx.restore();
};
