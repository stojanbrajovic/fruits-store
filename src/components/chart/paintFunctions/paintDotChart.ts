import { yToScreenY } from "./yToScreenY";
import { Point } from "./Point";

export interface PaintDotChartConfig {
	radius?: number;
	color?: string;
	outlineColor?: string;
	outlineThickness?: number;
}

export const DEFAULT_CONFIG = {
	radius: 10,
	color: "black",
	outlineColor: "grey",
	outlineThickness: 3
};

export const paintDotChart = (
	ctx: CanvasRenderingContext2D,
	values: Point[],
	configArg: PaintDotChartConfig = {}
) => {
	const config = { ...DEFAULT_CONFIG, ...configArg };
	ctx.save();

	ctx.fillStyle = config.color;
	ctx.strokeStyle = config.outlineColor;
	ctx.lineWidth = config.outlineThickness;

	values.forEach(value => {
		ctx.beginPath();
		ctx.arc(
			value.x,
			yToScreenY(ctx, value.y),
			config.radius,
			0,
			Math.PI * 2
		);
		ctx.fill();
		ctx.stroke();
	});

	ctx.restore();
};
