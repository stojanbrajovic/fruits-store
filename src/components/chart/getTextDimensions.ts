const getTextDimensions = (text: string, fontSize?: number) => {
	const span = document.createElement("span");
	span.innerHTML = text;

	span.style.position = "absolute";
	span.style.display = "inline-block";
	span.style.height = "auto";
	span.style.width = "auto";
	span.style.whiteSpace = "nowrap";

	if (fontSize) {
		span.style.fontSize = fontSize + "px";
		span.style.lineHeight = "1";
	}

	document.body.appendChild(span);

	const dimensions = {
		height: span.clientHeight,
		width: span.clientWidth
	};

	span.parentNode?.removeChild(span);

	return dimensions;
};

export default getTextDimensions;
