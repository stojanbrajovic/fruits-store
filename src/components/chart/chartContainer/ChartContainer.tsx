import * as React from "react";

import { GridLine, Grid } from "../GridLine";
import { RightChartInfo } from "./RightChartInfo";
import { BottomChartInfo } from "./BottomChartInfo";
import { CursorInfo, MousePosition } from "./CursorInfo";
import { defaultInfoConfig } from "../properties/InfoConfig";
import { SnapPosition, SnapPoint } from "./SnapPoint";
import {
	ChartContainerConfig,
	ValuePoint
} from "../properties/ChartProperties";
import {
	getVisibleRightInfoGridLines,
	getRightInfoMaxTextWidth,
	DEFAULT_RIGHT_CHART_INFO
} from "./getRightChartInfoLinesData";
import {
	getBottomChartInfoLinesData,
	DEFAULT_BOTTOM_CHART_INFO
} from "./getBottomChartInfoLinesData";
import {
	AdjustValuesResult,
	adjustValuesToChart,
	DEFAULT_ADJUSTED_VALUES
} from "../adjustValuesToChart";
import { adjustGrid } from "./adjustGrid";
import getTextDimensions from "../getTextDimensions";

const MAX_RENDERS_PER_SECOND = 60;
const RIGHT_INFO_PADDING = 5;

export interface ChartContainerProperties extends ChartContainerConfig {
	renderChart: (
		width: number,
		height: number,
		adjustValuesResult: AdjustValuesResult,
		grid: Grid
	) => React.ReactNode;
	pointsToFitIntoChart?: ValuePoint[];
}

interface State {
	verticalLineClosestToCursor: GridLine | null;
	cursorPosition: MousePosition | null;
	activeSnapPosition: SnapPosition | null;
	showSnapPositionTooltip: boolean;
}

export class ChartContainer extends React.Component<
	ChartContainerProperties,
	State
> {
	static defaultProps: Partial<ChartContainerConfig> = {
		values: [],
		snapDistance: 10,
		borderWidth: 1,
		grid: { horizontal: [], vertical: [] },
		infoConfig: defaultInfoConfig,
		yCoordinateToString: (pos: number) => "" + Math.round(pos)
	};

	private rightChartInfoData: {
		visibleLines: GridLine[];
		maxTextWidth: number;
	} = DEFAULT_RIGHT_CHART_INFO;
	private bootomChartInfoData: {
		lines: GridLine[];
		lineWidths: number[];
	} = DEFAULT_BOTTOM_CHART_INFO;
	private chartHeight: number = 0;
	private chartWidth: number = 0;
	private adjustedValuesResult: AdjustValuesResult = DEFAULT_ADJUSTED_VALUES;
	private adjustedGrid: Grid = { vertical: [], horizontal: [] };

	private shouldUpdate = false;
	private updateIntervalHandler: number = 0;
	private containerDiv: HTMLDivElement | null = null;

	constructor(props: ChartContainerProperties) {
		super(props);
		this.state = {
			cursorPosition: null,
			verticalLineClosestToCursor: null,
			activeSnapPosition: null,
			showSnapPositionTooltip: false
		};
	}

	// TODO change when moving to React 16.3 https://reactjs.org/blog/2018/03/29/react-v-16-3.html#component-lifecycle-changes
	componentWillReceiveProps(nextProps: ChartContainerProperties) {
		const props = this.props;
		if (
			nextProps.grid !== props.grid ||
			nextProps.values !== props.values ||
			nextProps.infoConfig !== props.infoConfig ||
			props.isChartContainerHidden !== nextProps.isChartContainerHidden
		) {
			this.adjustChartToContainerDimensions(nextProps);
		}
	}

	shouldComponentUpdate() {
		this.shouldUpdate = true;
		return false;
	}

	componentDidMount() {
		this.forceUpdate();
		this.updateIntervalHandler = window.setInterval(() => {
			if (this.shouldUpdate) {
				this.forceUpdate();
				this.shouldUpdate = false;
			}
		}, 1000 / MAX_RENDERS_PER_SECOND);
	}

	componentWillUnmount() {
		window.clearInterval(this.updateIntervalHandler);
	}

	render() {
		const props = this.props;

		const infoConfig = { ...defaultInfoConfig, ...props.infoConfig };

		return (
			<div
				ref={this.divContainerRef}
				style={{ height: props.height, width: props.width }}
			>
				{this.props.isChartContainerHidden ? (
					this.renderChart()
				) : (
					<>
						<div style={{ display: "flex" }}>
							{this.renderChartDiv()}
							{!this.props.infoConfig?.isRightInfoDisabled && (
								<RightChartInfo
									{...infoConfig}
									mouseYPosition={
										this.state.cursorPosition
											? this.state.cursorPosition.y
											: undefined
									}
									lines={this.rightChartInfoData.visibleLines}
									height={this.chartHeight}
									positionToString={this.yCoordinateToString}
									width={this.rightChartInfoData.maxTextWidth}
									paddingLeft={RIGHT_INFO_PADDING}
								/>
							)}
						</div>
						{!this.props.infoConfig?.isBottomInfoDisabled && (
							<BottomChartInfo
								{...infoConfig}
								lines={this.bootomChartInfoData.lines}
								highlightedLine={
									this.state.verticalLineClosestToCursor ||
									undefined
								}
								width={this.chartWidth}
							/>
						)}
					</>
				)}
			</div>
		);
	}

	private renderChart() {
		return this.props.renderChart(
			this.chartWidth,
			this.chartHeight,
			this.adjustedValuesResult,
			this.adjustedGrid
		);
	}

	private divContainerRef = (div: HTMLDivElement) => {
		const shouldAdjust = !this.containerDiv;
		this.containerDiv = div;

		if (shouldAdjust) {
			this.adjustChartToContainerDimensions();
		}
	};

	private adjustChartToContainerDimensions(props = this.props) {
		const div = this.containerDiv;
		if (!div) {
			return;
		}

		const divHeight = div.offsetHeight;
		const textHeight =
			props.isChartContainerHidden ||
			props.infoConfig?.isBottomInfoDisabled
				? 0
				: getTextDimensions("0", props.infoConfig?.fontSize).height;
		const chartHeight = divHeight - textHeight;

		const divWidth = div.offsetWidth;
		const rightTextWidth =
			props.isChartContainerHidden ||
			props.infoConfig?.isRightInfoDisabled
				? 0
				: getRightInfoMaxTextWidth(
						props.grid?.horizontal || [],
						props.infoConfig?.fontSize || 0,
						props.yCoordinateToString || (y => y + ""),
						chartHeight
				  ) + RIGHT_INFO_PADDING;
		const chartWidth = divWidth - rightTextWidth;

		if (
			this.chartHeight !== chartHeight ||
			this.chartWidth !== chartWidth
		) {
			this.chartHeight = chartHeight;
			this.chartWidth = chartWidth;

			this.adjustedValuesResult = adjustValuesToChart(
				props.pointsToFitIntoChart || props.values || [],
				chartWidth,
				chartHeight,
				props
			);
			if (props.pointsToFitIntoChart) {
				this.adjustedValuesResult.adjustedValues = (
					props.values || []
				).map(val => ({
					...val,
					x:
						val.x * this.adjustedValuesResult.xMultiplier +
						this.adjustedValuesResult.xDelta,
					y:
						val.y * this.adjustedValuesResult.yMultiplier +
						this.adjustedValuesResult.yDelta
				}));
			}
			this.adjustedGrid = adjustGrid(
				props.grid || {},
				this.adjustedValuesResult
			);

			this.bootomChartInfoData = getBottomChartInfoLinesData(
				this.adjustedGrid.vertical || [],
				props.infoConfig?.fontSize || 10,
				chartWidth,
				props.infoConfig?.gridValuesMargin
			);

			const visibleRightInfoLines = getVisibleRightInfoGridLines(
				this.adjustedGrid.horizontal || [],
				props.infoConfig?.fontSize || 0,
				chartHeight,
				props.infoConfig?.gridValuesMargin || 0
			);
			this.rightChartInfoData = {
				maxTextWidth: rightTextWidth,
				visibleLines: visibleRightInfoLines
			};

			this.forceUpdate();
		}
	}

	private yCoordinateToString = (absCoordinate: number) => {
		const adjustedValuesResult = this.adjustedValuesResult;
		const coordinate =
			(absCoordinate - adjustedValuesResult.yDelta) /
			adjustedValuesResult.yMultiplier;
		const { yCoordinateToString } = this.props;
		return yCoordinateToString
			? yCoordinateToString(coordinate)
			: absCoordinate + "";
	};

	private renderChartDiv() {
		return (
			<div
				style={{
					position: "relative",
					overflow: "hidden",
					width: this.chartWidth,
					height: this.chartHeight,
					borderStyle: "solid",
					borderColor: "grey",
					borderWidth: this.props.borderWidth
				}}
			>
				<div style={{ position: "absolute" }}>{this.renderChart()}</div>
				{!this.props.infoConfig?.isCursorDisabled && (
					<>
						<div style={{ position: "absolute" }}>
							{this.state.activeSnapPosition && (
								<SnapPoint
									{...this.props.infoConfig}
									height={this.chartHeight}
									width={this.chartWidth}
									point={this.state.activeSnapPosition}
									isTooltipVisible={
										this.state.showSnapPositionTooltip
									}
									snapPointColor={
										this.props.infoConfig
											?.cursorSelectionColor
									}
									tooltipTextColor={
										this.props.infoConfig
											?.cursorSelectionTextColor
									}
								/>
							)}
						</div>
						<div style={{ position: "absolute" }}>
							<CursorInfo
								width={this.chartWidth}
								height={this.chartHeight}
								onMouseMove={this.onMouseMove}
								onMouseLeave={() =>
									this.setState({
										cursorPosition: null,
										verticalLineClosestToCursor: null,
										activeSnapPosition: null
									})
								}
								mousePosition={
									this.state.cursorPosition || undefined
								}
								color={
									this.props.infoConfig?.cursorSelectionColor
								}
							/>
						</div>
					</>
				)}
			</div>
		);
	}

	private static MAX_DISTANCE_TO_ACTIVE_VERTICAL_LINE = 2;
	private onMouseMove = (mousePositionArg: MousePosition) => {
		const mousePosition = { ...mousePositionArg };

		const activeSnapPosition = this.getActiveSnapPosition(mousePosition.x);
		mousePosition.x = activeSnapPosition
			? activeSnapPosition.x
			: mousePosition.x;

		const closestLine = this.getVerticalLineClosestToCursor(
			mousePosition.x
		);

		const isCloseEnoughToSnap =
			activeSnapPosition &&
			Math.abs(mousePosition.y - activeSnapPosition.y) <
				(this.props.snapDistance || 0);
		const cursorPosition = isCloseEnoughToSnap
			? activeSnapPosition
			: mousePosition;

		const verticalLineClosestToCursor =
			closestLine &&
			Math.abs(closestLine.position - cursorPosition.x) <
				ChartContainer.MAX_DISTANCE_TO_ACTIVE_VERTICAL_LINE
				? closestLine
				: null;

		this.setState({
			cursorPosition,
			verticalLineClosestToCursor,
			activeSnapPosition,
			showSnapPositionTooltip: isCloseEnoughToSnap
		});
	};

	private getActiveSnapPosition(x: number) {
		return this.adjustedValuesResult.adjustedValues
			.slice()
			.sort(
				(value1, value2) =>
					Math.abs(value1.x - x) - Math.abs(value2.x - x)
			)[0];
	}

	private getVerticalLineClosestToCursor(x: number) {
		return (this.adjustedGrid.vertical || [])
			.slice()
			.sort(
				(line1, line2) =>
					Math.abs(line1.position - x) - Math.abs(line2.position - x)
			)[0];
	}
}
