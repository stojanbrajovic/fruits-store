import { GridLine } from "../GridLine";
import getTextDimensions from "../getTextDimensions";

export const DEFAULT_BOTTOM_CHART_INFO = {
	lines: [],
	lineWidths: []
};

export const getBottomChartInfoLinesData = (
	lines: GridLine[],
	fontSize: number,
	width: number,
	linesMargin = 0
) => {
	const visibleLines: GridLine[] = [];
	const visibleLinesTextWidths: number[] = [];
	lines
		.filter(line => line.position >= 0 && line.position <= width)
		.sort((line1, line2) => line1.position - line2.position)
		.forEach(value => {
			const prevValueTextWidth =
				visibleLinesTextWidths[visibleLinesTextWidths.length - 1];
			const thisValueTextWidth = getTextDimensions(
				"" + (value.label || value.position),
				fontSize
			).width;
			const renderPosition = value.position - thisValueTextWidth / 2;
			if (prevValueTextWidth) {
				const prevValue = visibleLines[visibleLines.length - 1];
				if (
					renderPosition >= 0 &&
					prevValue.position + prevValueTextWidth / 2 <
						renderPosition - linesMargin
				) {
					visibleLines.push(value);
					visibleLinesTextWidths.push(thisValueTextWidth);
				}
			} else if (renderPosition >= 0) {
				visibleLines.push(value);
				visibleLinesTextWidths.push(thisValueTextWidth);
			}
		});

	return { lines: visibleLines, lineWidths: visibleLinesTextWidths };
};
