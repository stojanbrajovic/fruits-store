import { AdjustValuesResult } from "../adjustValuesToChart";
import { Grid } from "../GridLine";

export const adjustGrid = (
	grid: Grid,
	adjustValuesResult: AdjustValuesResult
): Grid => {
	return {
		vertical: grid.vertical?.map(line => ({
			position:
				line.position * adjustValuesResult.xMultiplier +
				adjustValuesResult.xDelta,
			label: line.label || line.position + ""
		})),
		horizontal: grid.horizontal?.map(line => ({
			position:
				line.position * adjustValuesResult.yMultiplier +
				adjustValuesResult.yDelta,
			label: line.label
		}))
	};
};
