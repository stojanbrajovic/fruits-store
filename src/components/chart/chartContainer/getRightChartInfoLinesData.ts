import { GridLine } from "../GridLine";
import getTextDimensions from "../getTextDimensions";

export const DEFAULT_RIGHT_CHART_INFO = {
	visibleLines: [],
	maxTextWidth: 0
};

export const getRightInfoMaxTextWidth = (
	lines: GridLine[],
	textHeight: number,
	positionToString: (pos: number) => string,
	height: number
) => {
	const linesTextWidth = lines.map(
		point =>
			getTextDimensions(
				point.label || positionToString(point.position),
				textHeight
			).width
	);

	return Math.max.apply(
		null,
		linesTextWidth.concat([
			getTextDimensions(positionToString(0)).width,
			getTextDimensions(positionToString(height)).width
		])
	);
};

export const getVisibleRightInfoGridLines = (
	lines: GridLine[],
	fontSize: number,
	height: number,
	valuesMargin: number
) => {
	const textHeight = fontSize || getTextDimensions("0", fontSize).height;
	const visibleLines = [] as GridLine[];
	lines
		.filter(line => line.position >= 0 && line.position <= height)
		.sort((line1, line2) => line1.position - line2.position)
		.forEach(line => {
			const lastVisibleLine = visibleLines[visibleLines.length - 1];
			const lastVisibleLineTop = lastVisibleLine
				? lastVisibleLine.position + textHeight / 2
				: -Number.MAX_VALUE;
			const thisLineBottom = line.position - textHeight / 2;

			if (lastVisibleLineTop <= thisLineBottom - valuesMargin) {
				visibleLines.push(line);
			}
		});

	return visibleLines;
};
