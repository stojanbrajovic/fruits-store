import * as React from "react";
import getTextDimensions from "../getTextDimensions";

export interface SnapPosition {
	x: number;
	y: number;
	label?: string;
}

export interface SnapPointProperties {
	point: SnapPosition;

	width: number;
	height: number;

	isTooltipVisible?: boolean;
	snapPointRadius?: number;
	snapPointColor?: string;
	tooltipTextColor?: string;
}

const TOOLTIP_TO_SNAP_POINT_MARGIN = 10;
const Tooltip: React.SFC<SnapPointProperties> = props => {
	const point = props.point;
	const tooltipText = point.label || "";
	const textDimensions = getTextDimensions(tooltipText);

	const x =
		point.x +
		(point.x - textDimensions.width - TOOLTIP_TO_SNAP_POINT_MARGIN > 0
			? -textDimensions.width - TOOLTIP_TO_SNAP_POINT_MARGIN
			: TOOLTIP_TO_SNAP_POINT_MARGIN);

	const y =
		point.y +
		(point.y + textDimensions.height + TOOLTIP_TO_SNAP_POINT_MARGIN <
		props.height
			? TOOLTIP_TO_SNAP_POINT_MARGIN
			: -textDimensions.height - TOOLTIP_TO_SNAP_POINT_MARGIN);

	return (
		<div
			className="d-chart-tooltip"
			style={{
				position: "absolute",
				backgroundColor: props.snapPointColor,
				color: props.tooltipTextColor,
				bottom: y,
				left: x
			}}
		>
			{tooltipText}
		</div>
	);
};

const PointComponent: React.SFC<SnapPointProperties> = props => (
	<div
		className="d-chart-pointComponent"
		style={{
			position: "absolute",
			borderRadius: "50%",
			backgroundColor: props.snapPointColor,
			width: props.snapPointRadius,
			height: props.snapPointRadius,
			bottom: props.point.y - (props.snapPointRadius || 10) / 2,
			left: props.point.x - (props.snapPointRadius || 10) / 2
		}}
	/>
);

export const SnapPoint: React.SFC<SnapPointProperties> = props => (
	<div
		style={{
			position: "relative",
			height: props.height,
			width: props.width
		}}
	>
		<PointComponent {...props} />
		{props.isTooltipVisible && <Tooltip {...props} />}
	</div>
);
SnapPoint.defaultProps = {
	snapPointRadius: 10,
	snapPointColor: "blue",
	tooltipTextColor: "white"
};
