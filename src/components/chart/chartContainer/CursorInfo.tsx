import * as React from "react";

export interface MousePosition {
	x: number;
	y: number;
}

export interface CursorInfoProperties {
	height: number;
	width: number;

	mousePosition?: MousePosition;

	onMouseMove?: (point: MousePosition) => void;
	onMouseLeave?: VoidFunction;
	color?: string;
	thickness?: number;
}

const renderCursorLine = (
	props: CursorInfoProperties,
	position: number,
	isVertical = false
) => (
	<div
		className="d-chart-cursorInfo"
		style={{
			position: "absolute",
			height: isVertical ? props.height : props.thickness,
			backgroundColor: props.color,
			width: isVertical ? props.thickness : props.width,
			bottom: isVertical ? 0 : position,
			left: isVertical ? position : 0
		}}
	/>
);

const onMouseMove = (
	event: React.MouseEvent<HTMLDivElement>,
	{ height, onMouseMove }: CursorInfoProperties
) => {
	const divRectangular = event.currentTarget.getBoundingClientRect();
	const mousePosition = {
		x: event.clientX - divRectangular.left,
		y: height - (event.clientY - divRectangular.top)
	};

	onMouseMove && onMouseMove(mousePosition);
};

export const CursorInfo: React.SFC<CursorInfoProperties> = props => (
	<div
		style={{
			position: "relative",
			height: props.height,
			width: props.width
		}}
	>
		{props.mousePosition ? (
			<>
				{renderCursorLine(
					props,
					props.mousePosition.y - (props.thickness || 1) / 2
				)}
				{renderCursorLine(
					props,
					props.mousePosition.x - (props.thickness || 1) / 2,
					true
				)}
			</>
		) : null}
		<div
			onMouseMove={event =>
				props.onMouseMove && onMouseMove(event, props)
			}
			onMouseLeave={props.onMouseLeave}
			style={{
				position: "absolute",
				height: props.height,
				width: props.width
			}}
		/>
	</div>
);

CursorInfo.defaultProps = {
	color: "blue",
	thickness: 1
};
