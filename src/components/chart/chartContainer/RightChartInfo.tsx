import * as React from "react";
import { GridLine } from "../GridLine";
import { InfoConfig } from "../properties/InfoConfig";
import getTextDimensions from "../getTextDimensions";

export interface RightChartInfoProperties extends InfoConfig {
	lines: GridLine[];
	height: number;
	width: number;
	paddingLeft: number;

	mouseYPosition?: number;
	positionToString?: (yPosition: number) => string;
}

const aproxEqual = (num1: number, num2: number) => Math.abs(num1 - num2) < 1;

const getLabelForLine = (
	position: number,
	lines: GridLine[],
	positionToString: (yPosition: number) => string
) => {
	const lineAtPosition = lines.filter(line =>
		aproxEqual(line.position, position)
	)[0];
	const lineLabel = lineAtPosition && lineAtPosition.label;

	return lineLabel || positionToString(position);
};

export const RightChartInfo: React.SFC<RightChartInfoProperties> = ({
	fontSize,
	width,
	height,
	lines,
	positionToString = (pos: number) => "" + pos,
	textColor,
	paddingLeft,
	mouseYPosition,
	cursorSelectionColor,
	cursorSelectionTextColor
}) => {
	const textHeight = getTextDimensions("0", fontSize).height;

	return (
		<div
			style={{
				width: width,
				overflow: "visible",
				position: "relative",
				height: height
			}}
		>
			{lines.map(point => {
				const label = point.label || positionToString(point.position);

				return (
					<div
						key={point.position + label}
						style={{
							position: "absolute",
							bottom: point.position - textHeight / 2,
							fontSize: fontSize,
							color: textColor,
							paddingLeft: paddingLeft
						}}
					>
						{label}
					</div>
				);
			})}
			{(mouseYPosition || mouseYPosition === 0) && (
				<div
					className="d-chart-rightChartInfoCurrent"
					style={{
						position: "absolute",
						bottom: mouseYPosition - textHeight / 2,
						fontSize: fontSize,
						backgroundColor: cursorSelectionColor,
						color: cursorSelectionTextColor,
						paddingLeft: paddingLeft
					}}
				>
					{getLabelForLine(mouseYPosition, lines, positionToString)}
				</div>
			)}
		</div>
	);
};
RightChartInfo.defaultProps = {
	positionToString: (y: number) => "" + Math.round(y)
};
