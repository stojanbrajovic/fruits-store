import React, { useMemo } from "react";
import { GridLine } from "../GridLine";
import { InfoConfig, defaultInfoConfig } from "../properties/InfoConfig";
import getTextDimensions from "../getTextDimensions";

export interface BottomChartInfoConfig extends InfoConfig {
	lines: GridLine[];
	width: number;
}

export interface BottomChartInfoProperties extends BottomChartInfoConfig {
	highlightedLine?: GridLine;
}

export interface SingleLineComponentProperties {
	line: GridLine;
	width: number;
	className?: string;
	textColor?: string;
	backgroundColor?: string;
	fontSize?: number;
}
export const LineComponent: React.FC<SingleLineComponentProperties> = props => (
	<span
		className={props.className}
		style={{
			position: "absolute",
			left: props.line.position - props.width / 2,
			fontSize: props.fontSize,
			background: props.backgroundColor,
			color: props.textColor
		}}
	>
		{props.line.label || props.line.position}
	</span>
);

const getLineWidth = (line: GridLine, fontSize = defaultInfoConfig.fontSize) =>
	getTextDimensions("" + (line.label || line.position), fontSize).width;

export const BottomChartInfo: React.FC<BottomChartInfoProperties> = props => {
	const height = getTextDimensions("0").height;

	const lineWidths = useMemo(
		() => props.lines.map(line => getLineWidth(line, props.fontSize)),
		[props.lines, props.fontSize]
	);

	return (
		<div
			style={{
				position: "relative",
				width: props.width,
				height,
				overflow: "visible"
			}}
		>
			{props.lines.map((line, index) => (
				<LineComponent
					key={"" + line.position}
					line={line}
					width={lineWidths[index]}
					fontSize={props.fontSize}
					textColor={props.textColor}
				/>
			))}
			{props.highlightedLine && (
				<LineComponent
					line={props.highlightedLine}
					width={getLineWidth(props.highlightedLine, props.fontSize)}
					fontSize={props.fontSize}
					backgroundColor={props.cursorSelectionColor}
					textColor={props.cursorSelectionTextColor}
				/>
			)}
		</div>
	);
};
