import { GridLine, Grid } from "./GridLine";

interface ValuePoint {
	x: number;
}

export interface Value<PointType extends ValuePoint> {
	point: PointType;
	bottomLabel?: string;
}

export const orderedValuesToChartProperties = <PointType extends ValuePoint>(
	orderedValues: Value<PointType>[]
) => {
	const values: PointType[] = [];
	const verticalGrid: GridLine[] = [];

	orderedValues.forEach((value, index) => {
		values.push({ ...(value.point as any), x: index });
		verticalGrid.push({ position: index, label: value.bottomLabel });
	});

	const grid: Grid = {
		vertical: verticalGrid
	};

	return { values, grid };
};
