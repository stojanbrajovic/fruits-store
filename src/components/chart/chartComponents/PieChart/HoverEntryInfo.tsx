import * as React from "react";
import { PieChartEntry } from "./PieChartEntry";

export interface HoverEntryInfoProperties {
	entry: PieChartEntry;
	style?: React.CSSProperties;
}

export const HoverEntryInfo: React.SFC<HoverEntryInfoProperties> = ({
	entry,
	style
}) => <div style={style}>{entry.info || entry.value}</div>;
HoverEntryInfo.defaultProps = {
	style: {
		border: "solid",
		borderWidth: "0.2em",
		borderColor: "rgb(50, 50, 50)",
		backgroundColor: "rgba(120, 120, 120, 0.9)",
		padding: "0.3em",
		color: "black"
	}
};
