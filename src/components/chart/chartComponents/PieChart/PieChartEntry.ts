export interface PieChartEntry {
	value: number;

	info?: React.ReactNode;
	color?: string;
	onClick?: (event: React.MouseEvent<SVGPathElement>) => void;
	onMouseEnter?: (event: React.MouseEvent<SVGPathElement>) => void;
	onMouseLeave?: (event: React.MouseEvent<SVGPathElement>) => void;
}
