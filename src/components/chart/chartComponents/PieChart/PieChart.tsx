import * as React from "react";
import PieChartSVG, { PieChartSVGProperties } from "./PieChartSVG";
import { PieChartEntry } from "./PieChartEntry";
import { HoverEntryInfo } from "./HoverEntryInfo";

const HOVERED_ENTRY_INFO_OFFSET = 15;

export interface PieChartProperties extends PieChartSVGProperties {
	hoveredEntryInfoStyle?: React.CSSProperties;
}

interface State {
	hoveredEntry: PieChartEntry | null;
	mousePosition: { x: number; y: number };
	onMouseMove: null | ((event: React.MouseEvent<HTMLDivElement>) => void);
}

class PieChart extends React.Component<PieChartProperties, State> {
	static defaultProps = {
		outlineColor: "black",
		height: "100%",
		width: "100%"
	};

	state: State = {
		hoveredEntry: null,
		mousePosition: { x: 0, y: 0 },
		onMouseMove: null
	};

	render() {
		return (
			<div
				style={{
					height: this.props.height,
					width: this.props.width,
					position: "relative"
				}}
				onMouseMove={this.state.onMouseMove || undefined}
			>
				<PieChartSVG
					{...this.props}
					onEntryMouseEnter={this.onEntryMouseEnter}
					onEntryMouseLeave={this.onEntryMouseLeave}
				/>
				{this.state.hoveredEntry ? (
					<div
						style={{
							position: "absolute",
							top:
								this.state.mousePosition.y +
								HOVERED_ENTRY_INFO_OFFSET,
							left:
								this.state.mousePosition.x +
								HOVERED_ENTRY_INFO_OFFSET
						}}
					>
						<HoverEntryInfo
							entry={this.state.hoveredEntry}
							style={this.props.hoveredEntryInfoStyle}
						/>
					</div>
				) : null}
			</div>
		);
	}

	private onMouseMove = (event: React.MouseEvent<HTMLDivElement>) => {
		const divRectangular = event.currentTarget.getBoundingClientRect();
		const mousePosition = {
			x: event.clientX - divRectangular.left,
			y: event.clientY - divRectangular.top
		};
		this.setState({ mousePosition });
	};

	private onEntryMouseEnter = (
		hoveredEntry: PieChartEntry,
		event: React.MouseEvent<SVGPathElement>
	) => {
		this.setState({
			hoveredEntry,
			onMouseMove: this.onMouseMove
		});
	};

	private onEntryMouseLeave = (entry: PieChartEntry) => {
		this.setState({ hoveredEntry: null, onMouseMove: null });
	};
}

export default PieChart;
