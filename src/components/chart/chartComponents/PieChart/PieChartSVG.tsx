import * as React from "react";
import { PieChartEntry } from "./PieChartEntry";
import { SingleEntry } from "./SingleEntry";

export interface PieChartSVGProperties {
	entries: PieChartEntry[];

	height?: number | string;
	width?: number | string;
	outlineColor?: string;
	disableOutline?: boolean;
}

interface Callbacks {
	onEntryMouseEnter: (
		entry: PieChartEntry,
		event: React.MouseEvent<SVGPathElement>
	) => void;
	onEntryMouseLeave: (entry: PieChartEntry) => void;
}

const PieChartSVG: React.SFC<PieChartSVGProperties & Callbacks> = props => {
	const allValuesSum = props.entries.reduce(
		(result: number, entry) => result + entry.value,
		0
	);

	let drawnValesSum = 0;
	return (
		<svg
			height={props.height}
			width={props.width}
			viewBox="0 0 1 1"
			overflow="visible"
		>
			{props.entries.map((entry, index) => {
				const result = (
					<SingleEntry
						key={index}
						{...entry}
						valuesSum={allValuesSum}
						prevValuesSum={drawnValesSum}
						outlineColor={
							props.disableOutline
								? undefined
								: props.outlineColor
						}
						onMouseEnter={event => {
							props.onEntryMouseEnter(entry, event);
							entry.onMouseEnter && entry.onMouseEnter(event);
						}}
						onMouseLeave={event => {
							props.onEntryMouseLeave(entry);
							entry.onMouseLeave && entry.onMouseLeave(event);
						}}
						onClick={() => console.log(entry)}
					/>
				);
				drawnValesSum += entry.value;
				return result;
			})}
		</svg>
	);
};

export default PieChartSVG;
