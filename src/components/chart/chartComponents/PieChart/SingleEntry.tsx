import * as React from "react";
import { PieChartEntry } from "./PieChartEntry";

const CENTER = 0.5;
const RADIUS = 0.49;
const HOVER_RADIUS = 0.53;

const getArcCoordinate = (value: number, sum: number, radius: number) => {
	const valueRatio = value / sum;
	const angleRadians = Math.PI * 2 * valueRatio;
	const x = CENTER + radius * Math.cos(angleRadians);
	const y = CENTER + radius * Math.sin(angleRadians);
	return { x, y };
};

const getRandomColor = () => {
	const r = Math.random() * 255;
	const g = Math.random() * 255;
	const b = Math.random() * 255;
	return `rgb(${r}, ${g}, ${b})`;
};

export interface SingleEntryProperties extends PieChartEntry {
	valuesSum: number;
	prevValuesSum: number;
	outlineColor?: string;
}

interface State {
	isMouseInside: boolean;
	color: string;
}

export class SingleEntry extends React.Component<SingleEntryProperties, State> {
	static defaultProps: Partial<SingleEntryProperties> = {
		onClick: () => {}
	};

	constructor(props: SingleEntryProperties) {
		super(props);
		this.state = {
			isMouseInside: false,
			color: this.props.color || getRandomColor()
		};
	}

	render() {
		return this.props.value === this.props.valuesSum
			? this.renderCircle()
			: this.renderArc();
	}

	private renderCircle() {
		return (
			<circle
				cx={CENTER}
				cy={CENTER}
				r={RADIUS}
				{...this.getCommonProps()}
			/>
		);
	}

	private renderArc() {
		const props = this.props;
		const radius = this.state.isMouseInside ? HOVER_RADIUS : RADIUS;
		const arcStart = getArcCoordinate(
			props.prevValuesSum,
			props.valuesSum,
			radius
		);
		const arcEnd = getArcCoordinate(
			props.prevValuesSum + props.value,
			props.valuesSum,
			radius
		);

		const movePath = `M ${arcStart.x} ${arcStart.y}`;
		const largeArcFlag = props.value / props.valuesSum <= 0.5 ? "0" : "1";
		const arcPath = `A ${radius} ${radius} 0 ${largeArcFlag} 1 ${arcEnd.x} ${arcEnd.y}`;
		const linePath = `L ${CENTER} ${CENTER}`;
		const fullPath = `${movePath} ${arcPath} ${linePath} Z`;

		return <path d={fullPath} {...this.getCommonProps()} />;
	}

	private getCommonProps() {
		return {
			fill: this.state.color,
			stroke: this.props.outlineColor,
			strokeWidth: 0.01,
			onMouseEnter: this.onMouseEnter,
			onMouseLeave: this.onMouseLeave,
			onClick: this.props.onClick
		} as React.SVGAttributes<{}>;
	}

	private onMouseEnter = (event: React.MouseEvent<SVGPathElement>) => {
		this.props.onMouseEnter && this.props.onMouseEnter(event);
		this.setState({ isMouseInside: true });
	};

	private onMouseLeave = (event: React.MouseEvent<SVGPathElement>) => {
		this.props.onMouseLeave && this.props.onMouseLeave(event);
		this.setState({ isMouseInside: false });
	};
}
