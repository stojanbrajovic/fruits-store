import { SingleBarValue } from "../properties/BarProperties";

export const barValuesToYPadding = (values: SingleBarValue[]) => {
	if (values.length === 0) {
		return 0;
	}

	const closeExtremes = {
		min: Number.POSITIVE_INFINITY,
		max: Number.NEGATIVE_INFINITY
	};
	const verticalExtremes = {
		min: Number.POSITIVE_INFINITY,
		max: Number.NEGATIVE_INFINITY
	};

	values.forEach(bar => {
		closeExtremes.min = Math.min(bar.close, closeExtremes.min);
		closeExtremes.max = Math.max(bar.close, closeExtremes.max);

		const barVerticalValues = [bar.open, bar.close, bar.high, bar.low];
		const min = Math.min.apply(null, barVerticalValues);
		const max = Math.max.apply(null, barVerticalValues);

		verticalExtremes.min = Math.min(min, verticalExtremes.min);
		verticalExtremes.max = Math.max(max, verticalExtremes.max);
	});

	return Math.max(
		Math.abs(verticalExtremes.min - closeExtremes.min),
		Math.abs(verticalExtremes.max - closeExtremes.max)
	);
};
