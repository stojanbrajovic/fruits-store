import * as React from "react";
import {
	paintLineChart,
	PaintLineChartProperties
} from "../paintFunctions/paintLineChart";
import { ChartProperties } from "../properties/ChartProperties";
import { ChartWithPoints } from "./ChartWithPoints";
import { CommonCanvasProperties } from "../Canvas";

export interface LineChartProperties
	extends ChartProperties,
		CommonCanvasProperties,
		PaintLineChartProperties {}

export const LineChart: React.SFC<LineChartProperties> = props => (
	<ChartWithPoints {...props} paintFunction={paintLineChart} config={props} />
);
