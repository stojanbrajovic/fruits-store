import * as React from "react";
import {
	paintCandlestickChart,
	CandlestickProperties,
	DEFAULT_PROPS
} from "../paintFunctions/paintCandlestickChart";
import { SingleBarValue } from "../properties/BarProperties";
import { ChartProperties } from "../properties/ChartProperties";
import { ChartWithBars } from "./ChartWithBars";

export interface CandlestickChartProperties
	extends CandlestickProperties,
		ChartProperties<SingleBarValue> {}

export const CandlestickChart: React.SFC<CandlestickChartProperties> = props => (
	<ChartWithBars
		{...props}
		paintFunction={paintCandlestickChart}
		xPadding={
			(props.xPadding || 0) +
			(props.barWidth || DEFAULT_PROPS.barWidth) / 2
		}
	/>
);
CandlestickChart.defaultProps = DEFAULT_PROPS;
