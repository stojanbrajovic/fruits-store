import * as React from "react";
import { ChartProperties } from "../properties/ChartProperties";
import {
	PaintHistogramChartConfig,
	paintHistogram,
	DEFAULT_CONFIG,
	HistogramPoint
} from "../paintFunctions/paintHistogram";
import { ChartWithPoints } from "./ChartWithPoints";

export interface HistogramChartProperties
	extends ChartProperties<HistogramPoint>,
		PaintHistogramChartConfig {}

export const HistogramChart: React.SFC<HistogramChartProperties> = p => {
	const props = { xPadding: 0, yPadding: 0, ...DEFAULT_CONFIG, ...p };
	return (
		<ChartWithPoints
			{...props}
			config={props}
			paintFunction={paintHistogram}
			yPadding={props.outlineThickness + props.yPadding}
			xPadding={
				props.outlineThickness + props.barWidth / 2 + props.xPadding
			}
		/>
	);
};
HistogramChart.defaultProps = DEFAULT_CONFIG;
