import * as React from "react";
import {
	DEFAULT_PROPS,
	BarProperties,
	SingleBarValue
} from "../properties/BarProperties";
import { paintBarsChart } from "../paintFunctions/paintBarsChart";
import { ChartProperties } from "../properties/ChartProperties";
import { ChartWithBars } from "./ChartWithBars";

export interface BarsChartProperties
	extends BarProperties,
		ChartProperties<SingleBarValue> {}

export const BarsChart: React.SFC<BarsChartProperties> = props => (
	<ChartWithBars
		{...props}
		paintFunction={paintBarsChart}
		yPadding={
			(props.yPadding || 0) +
			(props.barWidth || DEFAULT_PROPS.barWidth) / 2
		}
		xPadding={
			(props.barWidth || DEFAULT_PROPS.barWidth) * 2 +
			(props.xPadding || 0)
		}
	/>
);
BarsChart.defaultProps = DEFAULT_PROPS;
