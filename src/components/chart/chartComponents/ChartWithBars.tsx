import * as React from "react";

import { ValuePoint, ChartProperties } from "../properties/ChartProperties";
import {
	SingleBarValue,
	BarProperties,
	DEFAULT_PROPS
} from "../properties/BarProperties";
import { AdjustValuesResult } from "../adjustValuesToChart";
import { chartPropsToContainerProps } from "../properties/propsToFullProps";
import ChartCanvas from "../ChartCanvas";
import { Grid } from "../GridLine";
import { ChartContainer } from "../chartContainer/ChartContainer";

const getAdjustedValues = (
	values: SingleBarValue[],
	adjustValuesResult: AdjustValuesResult
) =>
	values.map(bar => ({
		x: bar.x * adjustValuesResult.xMultiplier + adjustValuesResult.xDelta,
		high:
			bar.high * adjustValuesResult.yMultiplier +
			adjustValuesResult.yDelta,
		low:
			bar.low * adjustValuesResult.yMultiplier +
			adjustValuesResult.yDelta,

		open:
			bar.open * adjustValuesResult.yMultiplier +
			adjustValuesResult.yDelta,
		close:
			bar.close * adjustValuesResult.yMultiplier +
			adjustValuesResult.yDelta
	}));

const getBarsPointsToFitIntoChart = (values: SingleBarValue[]) =>
	values.reduce(
		(result: ValuePoint[], bar) =>
			result.concat([
				{ x: bar.x, y: bar.open },
				{ x: bar.x, y: bar.close },
				{ x: bar.x, y: bar.low },
				{ x: bar.x, y: bar.high }
			]),
		[]
	);

export interface ChartWithBarsProperties
	extends BarProperties,
		ChartProperties<SingleBarValue> {
	paintFunction: (
		ctx: CanvasRenderingContext2D,
		propsArg: BarProperties
	) => void;
	yPadding?: number;
	xPadding?: number;
}

export const ChartWithBars: React.SFC<ChartWithBarsProperties> = props => {
	const chartContainerProps = chartPropsToContainerProps(
		props,
		props.values.map(val => ({ x: val.x, y: val.close, label: val.label }))
	);

	const renderChart = (
		width: number,
		height: number,
		adjustValuesResult: AdjustValuesResult,
		grid: Grid
	) => (
		<ChartCanvas
			{...props}
			height={height}
			width={width}
			paintAboveGridFunctions={[
				ctx =>
					props.paintFunction(ctx, {
						...(props as BarProperties),
						values: getAdjustedValues(
							props.values,
							adjustValuesResult
						)
					})
			]}
			grid={{
				horizontal: (grid.horizontal || []).map(line => line.position),
				vertical: (grid.vertical || []).map(line => line.position)
			}}
		/>
	);

	return (
		<ChartContainer
			{...chartContainerProps}
			xPadding={props.xPadding}
			yPadding={props.yPadding}
			renderChart={renderChart}
			pointsToFitIntoChart={getBarsPointsToFitIntoChart(props.values)}
		/>
	);
};
ChartWithBars.defaultProps = {
	barWidth: DEFAULT_PROPS.barWidth,
	yPadding: 0,
	xPadding: 0
};
