import * as React from "react";
import { ChartProperties } from "../properties/ChartProperties";
import {
	PaintDotChartConfig,
	paintDotChart,
	DEFAULT_CONFIG
} from "../paintFunctions/paintDotChart";
import { ChartWithPoints } from "./ChartWithPoints";

export interface DotsChartProperties
	extends ChartProperties,
		PaintDotChartConfig {}

export const DotsChart: React.SFC<DotsChartProperties> = p => {
	const props = { ...DEFAULT_CONFIG, ...p };
	return (
		<ChartWithPoints
			{...props}
			paintFunction={paintDotChart}
			config={props}
			yPadding={props.radius + props.outlineThickness}
			xPadding={props.radius + props.outlineThickness}
		/>
	);
};
DotsChart.defaultProps = DEFAULT_CONFIG;
