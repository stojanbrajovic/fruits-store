import * as React from "react";
import {
	paintAreaChart,
	PaintAreaConfig
} from "../paintFunctions/paintAreaChart";
import { ChartProperties } from "../properties/ChartProperties";
import { ChartWithPoints } from "./ChartWithPoints";

export interface AreaChartProperties extends ChartProperties, PaintAreaConfig {}

export const AreaChart: React.SFC<AreaChartProperties> = props => (
	<ChartWithPoints {...props} paintFunction={paintAreaChart} config={props} />
);

AreaChart.defaultProps = {
	topColor: "rgba(0, 255, 0, 0.5)",
	bottomColor: "rgba(0, 255, 255, 0.5)",
	lineColor: "red",
	lineThickness: 10
};
