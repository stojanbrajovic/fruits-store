import * as React from "react";

import { ChartProperties, ValuePoint } from "../properties/ChartProperties";
import { chartPropsToContainerProps } from "../properties/propsToFullProps";
import { ChartContainer } from "../chartContainer/ChartContainer";
import { AdjustValuesResult } from "../adjustValuesToChart";
import ChartCanvas from "../ChartCanvas";
import { Grid } from "../GridLine";

export interface ChartWithPointsProperties<ConfigType> extends ChartProperties {
	config: ConfigType;
	pointsToFitIntoChart?: ValuePoint[];
	paintFunction: (
		ctx: CanvasRenderingContext2D,
		values: ValuePoint[],
		config: ConfigType
	) => void;
}

export const ChartWithPoints = <ConfigType extends {}>(
	props: ChartWithPointsProperties<ConfigType>
) => {
	const containerProps = chartPropsToContainerProps(props, props.values);

	const renderChart = (
		width: number,
		height: number,
		adjustValuesResult: AdjustValuesResult,
		grid: Grid
	) => (
		<ChartCanvas
			{...props}
			backgoundColor={props.backgroundColor}
			width={width}
			height={height}
			paintAboveGridFunctions={[
				(ctx: CanvasRenderingContext2D) =>
					props.paintFunction(
						ctx,
						adjustValuesResult.adjustedValues,
						props.config
					)
			]}
			grid={{
				horizontal: (grid.horizontal || []).map(line => line.position),
				vertical: (grid.vertical || []).map(line => line.position)
			}}
		/>
	);

	return (
		<ChartContainer
			renderChart={renderChart}
			{...containerProps}
			pointsToFitIntoChart={props.pointsToFitIntoChart}
		/>
	);
};
