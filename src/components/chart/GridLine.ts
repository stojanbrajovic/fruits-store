export interface GridLine {
	position: number;
	label?: string;
}

export interface Grid {
	horizontal?: GridLine[];
	vertical?: GridLine[];
}
