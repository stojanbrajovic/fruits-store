import * as React from "react";

export type PaintFunction = (context: CanvasRenderingContext2D) => void;

export interface CommonCanvasProperties {
	height: number;
	width: number;
	backgoundColor?: string;
}

export interface CanvasProperties extends CommonCanvasProperties {
	paintFunctions?: PaintFunction[];
}

class Canvas extends React.Component<CanvasProperties> {
	static defaultProps = {
		height: 100,
		width: 100,
		paintFunctions: [],
		backgoundColor: "white"
	};

	private ctx: CanvasRenderingContext2D | null = null;

	componentDidMount() {
		this.paint();
	}

	componentDidUpdate() {
		this.paint();
	}

	render() {
		return (
			<canvas
				ref={this.onCanvasRef}
				width={this.props.width}
				height={this.props.height}
			/>
		);
	}

	private paint() {
		const ctx = this.ctx;
		const width = this.props.width || Canvas.defaultProps.width;
		const height = this.props.height || Canvas.defaultProps.height;

		if (ctx) {
			ctx.save();
			ctx.clearRect(0, 0, width, height);

			ctx.fillStyle =
				this.props.backgoundColor || Canvas.defaultProps.backgoundColor;
			ctx.fillRect(0, 0, width, height);
			ctx.restore();

			if (this.ctx && this.props.paintFunctions) {
				this.props.paintFunctions.forEach(paint =>
					paint(this.ctx as CanvasRenderingContext2D)
				);
			}
		}
	}

	private onCanvasRef = (canvas: HTMLCanvasElement) => {
		this.ctx = canvas ? canvas.getContext("2d") : null;
	};
}

export default Canvas;
