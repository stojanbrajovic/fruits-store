interface Point {
	x: number;
	y: number;
}

const getExtremesForValues = (values: Point[], forcedYMinimum?: number) => {
	const max = { x: Number.NEGATIVE_INFINITY, y: Number.NEGATIVE_INFINITY };
	const min = { x: Number.POSITIVE_INFINITY, y: Number.POSITIVE_INFINITY };

	values.forEach(value => {
		max.y = Math.max(max.y, value.y);
		min.y = Math.min(
			min.y,
			value.y,
			forcedYMinimum || forcedYMinimum === 0
				? forcedYMinimum
				: Number.MAX_SAFE_INTEGER
		);
		max.x = Math.max(max.x, value.x);
		min.x = Math.min(min.x, value.x);
	});

	return { min, max };
};

export interface AdjustValuesToChartDimensionsConfig {
	xPadding?: number;
	yPadding?: number;
	forcedYMinimum?: number;
}
const DEFAULT_CONFIG: AdjustValuesToChartDimensionsConfig = {
	xPadding: 0,
	yPadding: 0
};

export interface AdjustValuesResult {
	xDelta: number;
	yDelta: number;
	xMultiplier: number;
	yMultiplier: number;
	adjustedValues: Point[];
}

export const DEFAULT_ADJUSTED_VALUES: AdjustValuesResult = {
	xDelta: 0,
	yDelta: 0,
	xMultiplier: 1,
	yMultiplier: 1,
	adjustedValues: []
};

export const adjustValuesToChart = <PointType extends Point = Point>(
	valuesArg: PointType[],
	width: number,
	height: number,
	config = DEFAULT_CONFIG
): AdjustValuesResult => {
	if (valuesArg.length === 0) {
		return DEFAULT_ADJUSTED_VALUES;
	}

	const values = valuesArg.slice().sort((val1, val2) => val1.x - val2.x);
	const extremes = getExtremesForValues(values, config.forcedYMinimum);

	const horizontalSpace = width - (config.xPadding || 0) * 2;
	const xMultiplier = horizontalSpace / (extremes.max.x - extremes.min.x);
	const xDelta = 0 - extremes.min.x * xMultiplier + (config.xPadding || 0);

	const verticalSpace = height - (config.yPadding || 0) * 2;
	const yMultiplier = verticalSpace / (extremes.max.y - extremes.min.y);
	const yDelta = 0 - extremes.min.y * yMultiplier + (config.yPadding || 0);

	const adjustedValues: PointType[] = values.map(val => ({
		...val,
		x: val.x * xMultiplier + xDelta,
		y: val.y * yMultiplier + yDelta
	}));

	return { xDelta, xMultiplier, yDelta, yMultiplier, adjustedValues };
};
