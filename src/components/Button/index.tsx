import React from "react";
import "./style.css";

type Properties = {
	disabled?: boolean;
	onClick?: VoidFunction;
};

const Button: React.FC<Properties> = ({ onClick, children, disabled }) => (
	<button
		className="fruit-store-button"
		onClick={onClick}
		disabled={disabled}
	>
		{children}
	</button>
);

export default Button;
