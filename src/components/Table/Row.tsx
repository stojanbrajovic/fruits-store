import React from "react";
import FruitRow from "../../types/FruitRow";
import dateToString from "../../date/dateToString";

type Properties = {
	data: FruitRow;
	fruitOrder: string[];
};

const Row: React.FC<Properties> = ({
	data: { date, fruitsSold },
	fruitOrder
}) => (
	<div className="row">
		<div>{dateToString(date)}</div>
		{fruitOrder.map(fruit => (
			<div key={fruit}>{fruitsSold[fruit]}</div>
		))}
	</div>
);

export default Row;
