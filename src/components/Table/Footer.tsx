import React from "react";
import Button from "../Button";

type Properties = {
	page: number;
	decreasePage: VoidFunction;
	numberOfRows: number;
	itemsPerPage: number;
	increasePage: VoidFunction;
	onItemsPerPageChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
};

const Footer: React.FC<Properties> = ({
	page,
	decreasePage,
	numberOfRows,
	itemsPerPage,
	increasePage,
	onItemsPerPageChange
}) => (
	<div className="footer">
		<span>
			<Button disabled={page === 0} onClick={decreasePage}>
				{"<"}
			</Button>
			<span>Page: {page + 1}</span>
			<Button
				disabled={numberOfRows <= itemsPerPage * (page + 1)}
				onClick={increasePage}
			>
				{">"}
			</Button>
		</span>
		<span>
			Items per page:
			<input
				value={itemsPerPage}
				type="number"
				onChange={onItemsPerPageChange}
				max={numberOfRows}
				min={1}
			/>
		</span>
	</div>
);

export default Footer;
