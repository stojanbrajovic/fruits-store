import React, { useState, useCallback, useEffect, useMemo } from "react";
import FruitRow from "../../types/FruitRow";
import Row from "./Row";
import Header from "./Header";
import Footer from "./Footer";

import "./style.css";
import FruitInfo from "../../types/FruitColor";

const DEFAULT_ITEMS_PER_PAGE = 10;

type Properties = {
	fruitsInfo: FruitInfo[];
	rowsData: FruitRow[];
};

const Table: React.FC<Properties> = ({ rowsData, fruitsInfo }) => {
	const [page, setPage] = useState(0);
	const [itemsPerPage, setItemsPerPage] = useState(
		Math.min(DEFAULT_ITEMS_PER_PAGE, rowsData.length)
	);

	useEffect(() => setPage(0), [rowsData]);

	const onItemsPerPageChange = useCallback(
		(event: React.ChangeEvent<HTMLInputElement>) => {
			const newValue = parseInt(event.target.value);
			if (newValue * page > rowsData.length) {
				setPage(Math.ceil(newValue / rowsData.length));
			}

			setItemsPerPage(newValue);
		},
		[page, rowsData.length]
	);
	const increasePage = useCallback(() => setPage(page + 1), [page]);
	const decreasePage = useCallback(() => setPage(page - 1), [page]);

	const dataToDisplay = useMemo(
		() => rowsData.slice(page * itemsPerPage, (page + 1) * itemsPerPage),
		[page, itemsPerPage, rowsData]
	);
	const fruitOrder = useMemo(() => fruitsInfo.map(info => info.name), [
		fruitsInfo
	]);

	return (
		<>
			<div className="table">
				<Header fruitColors={fruitsInfo} />
				{dataToDisplay.map(row => (
					<Row
						fruitOrder={fruitOrder}
						data={row}
						key={row.date.toDateString()}
					/>
				))}
			</div>
			<Footer
				decreasePage={decreasePage}
				increasePage={increasePage}
				itemsPerPage={itemsPerPage}
				numberOfRows={rowsData.length}
				onItemsPerPageChange={onItemsPerPageChange}
				page={page}
			/>
		</>
	);
};

export default Table;
