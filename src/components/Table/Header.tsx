import React from "react";
import FruitInfo from "../../types/FruitColor";

type Properties = {
	fruitColors: FruitInfo[];
};

const Header: React.FC<Properties> = ({ fruitColors }) => (
	<div className="row">
		<div>Date</div>
		{fruitColors.map(({ name, color }) => (
			<div key={name} style={{ backgroundColor: color }}>
				{name}
			</div>
		))}
	</div>
);

export default Header;
