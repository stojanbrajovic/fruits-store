import React, { useCallback } from "react";
import dateToString from "../date/dateToString";

type Properties = {
	date: Date;
	onChange: (date: Date) => void;
	maxDate?: Date;
};

const getOnDateChanged = (change: (date: Date) => void) => (
	event: React.ChangeEvent<HTMLInputElement>
) => {
	const date = new Date(event.target.value);
	if (isNaN(date.getTime())) {
		change(new Date());
		return;
	}

	change(date);
};

const DatePicker: React.FC<Properties> = ({ date, onChange, maxDate }) => {
	const onInputChanged = useCallback(getOnDateChanged(onChange), [onChange]);

	return (
		<input
			type="date"
			value={dateToString(date)}
			onChange={onInputChanged}
			max={maxDate ? dateToString(maxDate) : undefined}
		/>
	);
};

export default DatePicker;
