import React from "react";
import "./style.css";

const Loading: React.FC = () => (
	<div className="loading">
		<div />
	</div>
);

export default Loading;
