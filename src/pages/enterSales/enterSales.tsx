import React, { useState, useCallback, useMemo } from "react";
import Loading from "../../components/Loading";
import FruitInfo from "../../types/FruitColor";
import Button from "../../components/Button";
import mockFetch from "../../mockServer/mockFetch";
import DatePicker from "../../components/DatePicker";

import "./style.css";

type Properties = {
	fruitsInfo: FruitInfo[];
};

const EnterSalesPage: React.FC<Properties> = ({ fruitsInfo }) => {
	const [isSending, setIsSending] = useState(false);
	const [date, setDate] = useState(new Date());
	const [fruitsSold, setFruitsSold] = useState({} as Record<string, number>);

	const submitPrices = useCallback(() => {
		const sendPrices = async () => {
			setIsSending(true);
			await mockFetch();
			setIsSending(false);
		};
		sendPrices();
	}, []);

	const setFruisSoldMap = useMemo(
		() =>
			fruitsInfo.reduce(
				(result, { name }) => ({
					...result,
					[name]: (event: React.ChangeEvent<HTMLInputElement>) => {
						setFruitsSold({
							...fruitsSold,
							[name]: parseInt(event.target.value)
						});
					}
				}),
				{} as Record<
					string,
					(event: React.ChangeEvent<HTMLInputElement>) => void
				>
			),
		[fruitsInfo, fruitsSold]
	);

	return isSending ? (
		<Loading />
	) : (
		<div className="enter-sales-root">
			<div>
				<div>
					Date: <DatePicker date={date} onChange={setDate} />
				</div>
				{fruitsInfo.map(({ name }) => (
					<div key={name}>
						{name}:{" "}
						<input
							type="number"
							value={fruitsSold[name] || 0}
							onChange={setFruisSoldMap[name]}
						/>
					</div>
				))}
				<Button onClick={submitPrices}>Submit</Button>
			</div>
		</div>
	);
};

export default EnterSalesPage;
