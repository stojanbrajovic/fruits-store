import React, { useState, useEffect } from "react";
import FruitInfo from "../../types/FruitColor";
import FruitRow from "../../types/FruitRow";
import Datepickers from "./Datepickers";
import Loading from "../../components/Loading";
import Table from "../../components/Table";
import getReport from "../../mockServer/getReport";
import HomePageCharts from "./HomePageCharts";
import "./style.css";

type Properties = {
	fruitColors: FruitInfo[];
};

const ReportPage: React.FC<Properties> = ({ fruitColors }) => {
	const [startDate, setStartDate] = useState(new Date());
	const [endDate, setEndDate] = useState(new Date());
	const [tableData, setTableData] = useState([] as FruitRow[]);

	useEffect(() => {
		setTableData([]);
		const fetchReport = async () => {
			const data = await getReport(startDate, endDate);
			setTableData(data);
		};
		fetchReport();
	}, [startDate, endDate]);

	return (
		<div>
			<Datepickers
				startDate={startDate}
				onStartDateChanged={setStartDate}
				endDate={endDate}
				onEndDateChanged={setEndDate}
			/>
			{tableData.length === 0 ? (
				<Loading />
			) : (
				<div>
					<Table rowsData={tableData} fruitsInfo={fruitColors} />
					<HomePageCharts
						fruitColors={fruitColors}
						fruitSaleData={tableData}
					/>
				</div>
			)}
		</div>
	);
};

export default ReportPage;
