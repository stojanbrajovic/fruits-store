import React from "react";
import DatePicker from "../../components/DatePicker";

type Properties = {
	endDate: Date;
	startDate: Date;
	onEndDateChanged: (date: Date) => void;
	onStartDateChanged: (date: Date) => void;
};

const Datepickers: React.FC<Properties> = ({
	startDate,
	onStartDateChanged,
	endDate,
	onEndDateChanged
}) => (
	<div className="datepickers">
		From{" "}
		<DatePicker
			date={startDate}
			onChange={onStartDateChanged}
			maxDate={endDate}
		/>{" "}
		to <DatePicker date={endDate} onChange={onEndDateChanged} />
	</div>
);

export default Datepickers;
