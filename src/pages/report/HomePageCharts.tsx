import React, { useMemo } from "react";
import PieChart from "../../components/chart/chartComponents/PieChart/PieChart";
import FruitInfo from "../../types/FruitColor";
import FruitRow from "../../types/FruitRow";
import { HistogramChart } from "../../components/chart/chartComponents";
import { Grid } from "../../components/chart/GridLine";

const CHART_PADDING = 10;
const PIE_CHART_SIZE = 300;
const HISTOGRAM_HEIGHT = 250;
const HISTOGRAM_WIDTH = 400;

type Properties = {
	fruitColors: FruitInfo[];
	fruitSaleData: FruitRow[];
};

const getTotalsByFruit = (fruitSaleData: FruitRow[]) =>
	fruitSaleData.reduce((res, { fruitsSold }) => {
		const result = { ...res };
		Object.keys(fruitsSold).forEach(fruit => {
			const sold = fruitsSold[fruit];
			const oldTotal = result[fruit] || 0;
			result[fruit] = oldTotal + sold;
		});
		return result;
	}, {} as Record<string, number>);

const getPieChartEntries = (
	fruitColors: FruitInfo[],
	totalsByFruit: Record<string, number>
) =>
	fruitColors.map(({ name, color }) => {
		const value = totalsByFruit[name];
		return {
			info: `${name}: ${value}`,
			color,
			value
		};
	});

const getHistogramValues = (
	fruitColors: FruitInfo[],
	totalsByFruit: Record<string, number>
) =>
	fruitColors.map(({ name, color }, index) => ({
		x: index,
		y: totalsByFruit[name],
		label: `${name}: ${totalsByFruit[name]}`,
		color
	}));

const createGrid = (
	fruitColors: FruitInfo[],
	totalsByFruit: Record<string, number>
): Grid => {
	const result: Grid = { vertical: [], horizontal: [] };

	result.vertical = fruitColors.map(({ name }, index) => ({
		position: index,
		label: name
	}));

	const max = Math.max(...Object.values(totalsByFruit));
	for (let i = 0; i < max + CHART_PADDING; i += 10) {
		result.horizontal?.push({
			position: i
		});
	}

	return result;
};

const HomePageCharts: React.FC<Properties> = ({
	fruitColors,
	fruitSaleData
}) => {
	const totalsByFruit = useMemo(() => getTotalsByFruit(fruitSaleData), [
		fruitSaleData
	]);

	const pieChartEntries = useMemo(
		() => getPieChartEntries(fruitColors, totalsByFruit),
		[fruitColors, totalsByFruit]
	);

	const histogramValues = useMemo(
		() => getHistogramValues(fruitColors, totalsByFruit),
		[fruitColors, totalsByFruit]
	);

	const grid = useMemo(() => createGrid(fruitColors, totalsByFruit), [
		fruitColors,
		totalsByFruit
	]);

	return (
		<div className="home-page-charts">
			<div>
				<PieChart
					entries={pieChartEntries}
					width={Math.min(PIE_CHART_SIZE, window.innerWidth)}
				/>
			</div>
			<br />
			<div>
				<HistogramChart
					values={histogramValues}
					height={HISTOGRAM_HEIGHT}
					width={Math.min(HISTOGRAM_WIDTH, window.innerWidth)}
					color="darkgreen"
					infoConfig={{
						cursorSelectionColor: "darkgreen"
					}}
					grid={grid}
					xPadding={CHART_PADDING}
					yPadding={CHART_PADDING}
					outlineColor="black"
					gridColors="transparent"
					barWidth={20}
					forcedYMinimum={0}
				/>
			</div>
		</div>
	);
};

export default HomePageCharts;
