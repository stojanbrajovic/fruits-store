import React, { useState, useEffect } from "react";
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
	Redirect
} from "react-router-dom";
import Loading from "./components/Loading";
import FruitInfo from "./types/FruitColor";
import getFruitInfos from "./mockServer/getGruitInfos";
import ReportPage from "./pages/report";
import EnterSalesPage from "./pages/enterSales/enterSales";
import { REPORT, ENTER_SALES, DEFAULT_ROUTE } from "./routes";
import "./style.css";

const App: React.FC = () => {
	const [fruitsInfo, setFruitsInfo] = useState([] as FruitInfo[]);

	useEffect(() => {
		const fetchInfo = async () => {
			const info = await getFruitInfos();
			setFruitsInfo(info);
		};
		fetchInfo();
	}, []);

	return (
		<Router>
			<div className="root-container">
				<div className="app-container">
					{fruitsInfo.length === 0 ? (
						<Loading />
					) : (
						<div>
							<nav>
								<ul>
									<li>
										<Link to={REPORT}>Report</Link>
									</li>
									<li>
										<Link to={ENTER_SALES}>
											Enter sales
										</Link>
									</li>
								</ul>
							</nav>

							<Switch>
								<Route path={REPORT}>
									<ReportPage fruitColors={fruitsInfo} />
								</Route>
								<Route path={ENTER_SALES}>
									<EnterSalesPage fruitsInfo={fruitsInfo} />
								</Route>
								<Route path="/">
									<Redirect to={DEFAULT_ROUTE} />
								</Route>
							</Switch>
						</div>
					)}
				</div>
			</div>
		</Router>
	);
};

export default App;
